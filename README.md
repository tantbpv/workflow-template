# README #

* Start template
* ver. 1.0.0

# Get started #
* Instal node.js and npm (nodejs.org)
* $ sudo npm i gulp -g # install gulp globally
* $ cd /project_dir/ # go to project directory
* $ sudo npm i # install all dev dependency modules
* $ gulp # start gulp

# BEM and CSS selectors rule #
* .block;
* .block_element;
* .block_element--modifier;
* .js-script-name - javascript classes;
* .is-hidden, .is-active, .is-valid - javascript state classes;

# JS variable names rule #
* var $variable - jQuery object;

# Code style guide #
http://sadcitizen.me/code-guide/


