var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');
var plumber = require('gulp-plumber');
var notify = require("gulp-notify");
var watch = require('gulp-watch');
var rename = require("gulp-rename");
var livereload = require('gulp-livereload');
var connect = require('gulp-connect');
var sass = require('gulp-sass');
var jshint = require('gulp-jshint');
var fileinclude = require('gulp-file-include');
var concat = require('gulp-concat');

var path = {
	build: {
		html: 'build/',
		js: 'build/js/',
		css: 'build/css/',
		img: 'build/img/',
		fonts: 'build/fonts/',
		libs: 'build/libs/'
	},
	src: {
		html: 'src/*.*',
		js: 'src/js/*.js',
		style: 'src/style/*.scss',
		img: 'src/img/**/*.*',
		fonts: 'src/fonts/**/*.*',
		libs: 'src/libs/**/*.*'
	},
	watch: {
		html: 'src/**/*.html',
		js: 'src/js/**/*.js',
		style: 'src/style/**/*.scss',
		img: 'src/img/**/*.*',
		fonts: 'src/fonts/**/*.*',
		libs: 'src/libs/**/*.*'
	},

};

gulp.task('lint', function () {
	return gulp.src('src/js/**/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

gulp.task('connect', function () {
	return connect.server({
		root: 'build',
		port: 8888,
		livereload: true
	});
});

gulp.task('html:build', function () {
	return gulp.src(path.src.html)
		.pipe(plumber())
		.pipe(fileinclude({
			prefix: '@@',
			basepath: '@file'
		}))
		.pipe(gulp.dest(path.build.html))
		.pipe(notify("html ready!"))
		.pipe(connect.reload());
});

gulp.task('js:build', function () {
	return gulp.src(path.src.js)
		.pipe(sourcemaps.init())
		.pipe(plumber())
		.pipe(concat('main.js'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.build.js))
		.pipe(notify("js ready!"))
		.pipe(connect.reload());
});

gulp.task('style:build', function () {
	return gulp.src(path.src.style)
		.pipe(sourcemaps.init())
		.pipe(plumber())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(minifyCSS())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.build.css))
		.pipe(notify("css ready!"))
		.pipe(connect.reload());
});

gulp.task('image:build', function () {
	return gulp.src(path.src.img)
		.pipe(plumber())
		.pipe(gulp.dest(path.build.img));
});

gulp.task('fonts:build', function () {
	return gulp.src(path.src.fonts)
		.pipe(plumber())
		.pipe(gulp.dest(path.build.fonts));
});
gulp.task('libs:build', function () {
	return gulp.src(path.src.libs)
		.pipe(plumber())
		.pipe(gulp.dest(path.build.libs));
});


gulp.task('build', [
	'html:build',
	'js:build',
	'style:build',
	'fonts:build',
	'image:build',
	'libs:build'
]);

gulp.task('watch', function () {
	watch([path.watch.html], function (event, cb) {
		gulp.start('html:build');
	});
	watch([path.watch.style], function (event, cb) {
		gulp.start('style:build');
	});
	watch([path.watch.js], function (event, cb) {
		gulp.start('js:build');
		gulp.start('lint');
	});
	
	/*
	watch([path.watch.img], function (event, cb) {
		gulp.start('image:build');
	});
	watch([path.watch.fonts], function (event, cb) {
		gulp.start('fonts:build');
	});
	watch([path.watch.libs], function (event, cb) {
		gulp.start('libs:build');
	});
	*/
});

// start task
gulp.task('default', ['build', 'connect', 'watch', 'lint']);