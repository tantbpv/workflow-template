// incr-decr-input.js
(function ($) {
	// prived methods

	var createBtns = function () {
		var $plusBtn = $('<i class="quantity_btn plus">+</i>');
		var $minusBtn = $('<i class="quantity_btn minus">-</i>');
		$(this).append($plusBtn);
		$(this).prepend($minusBtn);
		//console.log("btn create");
	};

	var initBtn = function (settings) {
			var $input = $(this).find("input");
			//console.log(settings);

			$(this).find(".q-inpt-btn").on("click", function () {
				var $button = $(this);
				var oldValue = $input.val() || settings.initValue;
				var newVal;

				// check increase or decrease value
				if ($button.hasClass("plus")) {
						newVal = parseFloat(oldValue) + settings.iterValue;
				} else { //$button.hasClass("minus")
						newVal = parseFloat(oldValue) - settings.iterValue;
				}

				// check for max and min limit
				newVal = checkLimits(newVal, settings);

				// fire trigger event
				$input.val(newVal).trigger("change");
			});
		};
	var onChange = function (settings) {
		var $input = $(this).find("input");
		$input.on("change", function () {
			$(this).val( checkLimits($(this).val(), settings) );
		});

	};
	var checkLimits = function (value, settings) {
		// check for max and min limit
		if ( value >= settings.maxValue && settings.maxValue !== false ) {
			value = settings.maxValue;
		}
		if ( value <= settings.minValue && settings.minValue !== false ) {
			value = settings.minValue;
		}
		return value;
	};

		// prived methods end
	var methods = {
		init: function (options) {

			// default option
			var settings = $.extend({
				iterValue: 1, // number
				initValue: 1, // number
				minValue: false, // number or false
				maxValue: false // number or false
			}, options);



			//console.log("init fn");

			return this.each(function () {
				var $this = $(this),
					init = "init",
					data = $this.data("numberInput");

				// Если плагин ещё не проинициализирован
				if (!data) {

					createBtns.apply(this, [settings]);
					initBtn.apply(this, [settings]);
					onChange.apply(this, [settings]);

					$(this).data("numberInput", init);

				}
			});
		},
		set: function (value) {
			return this.each(function () {
				var $this = $(this),
					input = $this.find("input");
				input.val(value);
			});
		},
		
		get: function () {
			var value = [];
			this.each(function () {
				var $this = $(this),
				input = $this.find("input");
				value.push( +input.val() );
			});
			return value;
		}
		
	};

	$.fn.numberInput = function (method) {

		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Метод с именем ' + method + ' не существует для jQuery.numberInput');
		}

	};

})(jQuery);


$(document).ready(function () {
	// tsr-quantity-forms increment init
	$(".js-quantity-inpt").numberInput({
		iterValue: 1, 
		initValue: 1, 
		minValue: 1,
		maxValue: 999
	});
	
}); // ready


// incr-decr-input.js end